package com.dam1.repaso;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaController implements ActionListener/*, KeyListener*/ {

    private VentanaModel model;
    private Ventana view;

    private int posicion;

    public VentanaController(VentanaModel model, Ventana view) {
        this.model = model;
        this.view = view;
        anadirActionListener(this);
        //anadirKeyListener(this);

        posicion = 0;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        String actionCommand = event.getActionCommand();
        Coche coche = null;

        switch (actionCommand) {
            case "Nuevo":
                view.tfMarca.setText("");
                view.tfMarca.setEditable(true);
                view.tfModelo.setText("");
                view.tfModelo.setEditable(true);
                view.tfMatricula.setText("");
                view.tfMatricula.setEditable(true);
                view.tfDniPropietario.setText("");
                view.tfDniPropietario.setEditable(true);

                view.btGuardar.setEnabled(true);
                break;
            case "Guardar":

                if (view.tfMarca.getText().equals("")) {
                    Util.mensajeError("La marca es un campo obligatorio", "Nuevo Coche");
                    return;
                }

                coche = new Coche();
                coche.setMarca(view.tfMarca.getText());
                coche.setModelo(view.tfDniPropietario.getText());
                coche.setMatricula(view.tfModelo.getText());
                coche.setDniPropietario(view.tfMatricula.getText());

                model.guardar(coche);

                view.btGuardar.setEnabled(false);
                break;
            case "Modificar":
                coche = new Coche();
                coche.setMarca(view.tfMarca.getText());
                coche.setModelo(view.tfModelo.getText());
                coche.setMatricula(view.tfMatricula.getText());
                coche.setDniPropietario(view.tfDniPropietario.getText());

                model.modificar(coche);
                break;
            case "Cancelar":
                view.tfMarca.setEditable(false);
                view.tfModelo.setEditable(false);
                view.tfMatricula.setEditable(false);
                view.tfDniPropietario.setEditable(false);

                coche = model.getActual();
                cargar(coche);

                view.btGuardar.setEnabled(false);
                break;
            case "Eliminar":
                if (JOptionPane.showConfirmDialog(null, "¿Está seguro?", "Eliminar", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                    return;

                model.eliminar();
                coche = model.getActual();
                cargar(coche);
                break;
/*            case "Buscar":
                coche = model.buscar(view.tfBusqueda.getText());
                if (coche == null) {
                    Util.mensajeInformacion("No se ha encontrado ningún coche con ese nombre", "Buscar");
                    return;
                }
                cargar(coche);
                break;*/
            case "|<":
                coche = model.getPrimero();
                cargar(coche);
                break;
            case "<":
                coche = model.getAnterior();
                cargar(coche);
                break;
            case ">":
                coche = model.getSiguiente();
                cargar(coche);
                break;
            case ">|":
                coche = model.getUltimo();
                cargar(coche);
                break;
            default:
                break;
        }
    }

/*    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getSource() == view.tfBusqueda) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                view.btBuscar.doClick();
            }
        }
    }*/

    /**
     * Carga los datos de un coche en la vista
     * @param coche
     */
    private void cargar(Coche coche) {
        if (coche == null)
            return;

        view.tfMarca.setText(coche.getMarca());
        view.tfModelo.setText(coche.getMatricula());
        view.tfMatricula.setText(coche.getModelo());
        view.tfDniPropietario.setText(String.valueOf(coche.getDniPropietario()));
    }

/*    private void anadirKeyListener(KeyListener listener) {
        view.tfBusqueda.addKeyListener(listener);
    }
*/
    private void anadirActionListener(ActionListener listener) {

        view.btNuevo.addActionListener(listener);
        view.btGuardar.addActionListener(listener);
        view.btModificar.addActionListener(listener);
        view.btEliminar.addActionListener(listener);
        view.btPrimero.addActionListener(listener);
        view.btAnterior.addActionListener(listener);
        view.btSiguiente.addActionListener(listener);
        view.btUltimo.addActionListener(listener);
 //       view.btBuscar.addActionListener(listener);
    }
}

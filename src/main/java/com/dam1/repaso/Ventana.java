package com.dam1.repaso;

import javax.swing.*;
import java.awt.*;

public class Ventana {
    private JPanel panel1,panel2,panel3;
    JTextField tfMarca;
    JTextField tfModelo;
    JTextField tfMatricula;
    JTextField tfDniPropietario;
    JLabel lMarca;
    JLabel lModelo;
    JLabel lMatricula;
    JLabel lDniPropietario;
    JButton btGuardar;
    JButton btNuevo;
    JButton btModificar;
    JButton btEliminar;
    JButton btAnterior;
    JButton btSiguiente;
    JButton btPrimero;
    JButton btUltimo;
    JBarraEstado barraEstado;
//    JTextField tfBusqueda;
//    JButton btBuscar;

    public Ventana() {

        JFrame frame = new JFrame("Coches");
        panel1 = new JPanel(new GridLayout(0,1));
        frame.setContentPane(panel1);
        panel2 = new JPanel(new GridLayout(4,2));
        panel3 = new JPanel(new GridLayout(2,4));
        panel1.add(panel2); panel1.add(panel3);
        lMarca = new JLabel("Marca:");
        tfMarca = new JTextField("");
        panel2.add(lMarca); panel2.add(tfMarca);
        lModelo = new JLabel("Modelo:");
        tfModelo = new JTextField("");
        panel2.add(lModelo); panel2.add(tfModelo);
        lMatricula = new JLabel("Matrícula:");
        tfMatricula = new JTextField("");
        panel2.add(lMatricula); panel2.add(tfMatricula);
        lDniPropietario = new JLabel("DNI del propietario:");
        tfDniPropietario = new JTextField("");
        panel2.add(lDniPropietario); panel2.add(tfDniPropietario);
        btNuevo = new JButton("Nuevo");
        btGuardar = new JButton("Guardar");
        btModificar = new JButton("Modificar");
        btEliminar = new JButton("Eliminar");
        btPrimero = new JButton("|<");
        btAnterior = new JButton("<");
        btSiguiente = new JButton(">");
        btUltimo = new JButton(">|");
        panel3.add(btGuardar);  panel3.add(btNuevo);  panel3.add(btModificar);  panel3.add(btEliminar);
        panel3.add(btPrimero);  panel3.add(btAnterior);  panel3.add(btSiguiente);  panel3.add(btUltimo);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}

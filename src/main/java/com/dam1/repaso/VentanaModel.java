package com.dam1.repaso;

import java.util.ArrayList;
/* ESTA ES LA CLASE A MODIFICAR

EN EL EJEMPLO ESTÁ CON UNA LISTA DE COCHES EN ARRAY.
ES DECIR, NO ES UN MODELO DE DATOS PERSISTENTE.
A LO LARGO DEL EJERCICIOS VAMOS A IR CAMBIANDO EL MODELO DE DATOS PARA QUE SEA:
1. FICHEROS DE TEXTO/BINARIOS
2. FICHEROS XML
3. EN BASE DE DATOS RELACIONAL POSTGRESQL O MYSQL SIN MAPEADO
4. MAPEADO CON HIBERNATE (MAPEADO ORM)
5. CON MONGODB  (BASE DE DATOS BASADA EN DOCUMENTOS)
6. CON OBJECTDB (BASE DE DATOS ORIENTADA A OBJETOS)
 */
public class VentanaModel {

    private ArrayList<Coche> listaCoches;
    private int posicion;

    public VentanaModel() {
        listaCoches = new ArrayList<>();
        posicion = 0;
    }

    /**
     * Guarda un coche en la lista
     * @param coche
     */
    public void guardar(Coche coche) {

        listaCoches.add(coche);
        posicion++;
        System.out.println(posicion);
        for (Coche a: listaCoches)
            System.out.println(a);
    }

    /**
     * Modifica los datos del animal actual
     * @param cocheModificado
     */
    public void modificar(Coche cocheModificado) {

        Coche coche = listaCoches.get(posicion);
        coche.setMarca(cocheModificado.getMarca());
        coche.setMatricula(cocheModificado.getMatricula());
        coche.setModelo(cocheModificado.getModelo());
        coche.setDniPropietario(cocheModificado.getDniPropietario());
    }

    /**
     * Elimina el animal actual
     */
    public void eliminar() {
        listaCoches.remove(posicion);
    }

    public Coche getActual() {

        return listaCoches.get(posicion);
    }

    /**
     * Busca un coche en la lista
     * @param marca del coche
     * @return El coche o null si no se ha encontrado nada
     */
/*    public Coche buscar(String marca) {
        for (Coche coche : listaCoches) {
            if (coche.getMarca().equals(marca)) {
                return coche;
            }
        }

        return null;
    }*/

    /**
     * Obtiene el coche que está en primera posición en la lista
     * @return
     */
    public Coche getPrimero() {

        posicion = 0;
        return listaCoches.get(posicion);
    }

    /**
     * Obtiene el animal que está en la posición anterior a la actual
     * @return
     */
    public Coche getAnterior() {

        if (posicion == 0)
            return null;

        posicion--;
        return listaCoches.get(posicion);
    }

    /**
     * Obtiene el animal que está en la posición siguiente a la actual
     * @return
     */
    public Coche getSiguiente() {

        if (posicion == listaCoches.size() - 1)
            return null;

        posicion++;
        return listaCoches.get(posicion);
    }

    /**
     * Obtiene el animal que está en la última posición de la lista
     * @return
     */
    public Coche getUltimo() {

        posicion = listaCoches.size() - 1;
        return listaCoches.get(posicion);
    }
}
